from setuptools import setup
from setuptools import find_packages

from version import __version__

setup(
        name='proteUs',
        packages=find_packages(),
        url='',
        license='',
        author='gkurgan',
        author_email='gkurgan@asu.edu',
        description='Protein engineering software suite',
        install_requires=[  'biopython'
                          ],  # please alphabetize requirements.
        #package_data={
        #    'seqtools': ['test/data/*', 'vcf_utils/*']
        #},
        version=__version__,
        entry_points={'console_scripts': [
            'proteUs = proteUs.parsing:main',
        ]},
        classifiers=['Topic :: Scientific/Engineering :: Bio-Informatics',
                     'Programming Language :: Python :: 3.5',
                     'Operating System :: POSIX',
                     'Operating System :: Unix',
                     'Operating System :: MacOS',
                     'Operating System :: Microsoft :: Windows'],
)
