from ._base import *
from .DataModel import *
from .Search import *
from .MSA import *
from ..tree import PhyML

@requires(MakeMSA)
class PhyMLTree(luigi.Task, PhyML):
    def run(self):
        self.run_phyml(self.input()["phy"].path, self.DataType, self.Bootstrap, self.SubModel)
    def output(self):
        return {"phyml" : luigi.LocalTarget(self.phyml_filename(self.input()["phy"].path)
                                                          )
                                            }
@requires(PhyMLTree)                                                  
class MakeTree(luigi.Task, PhyML):
    trees = {"phyml" :PhyMLTree}
    Tree = luigi.Parameter()
    def requires(self):
        self.clone(self.trees[self.Tree])
    def run(self):
    	pass
        #outfile = self.OutputPath + "tree_complete.txt"
        #with open(outfile, "w") as f:
        	#print("tree_complete")
        	#f.close()
    def output(self):
        #outfile = self.OutputPath + "tree_complete.txt"
        #return {"check" : luigi.LocalTarget(outfile)}
        pass
