#!/usr/bin/env python3
from ._base import *

class TargetPath(luigi.WrapperTask):
    TargetPath = luigi.Parameter()
    #Target path is a path to a fasta file that contains the nucleotide sequence of interest
    def output(self):
        return luigi.LocalTarget(self.TargetPath)

class Mode(luigi.WrapperTask):
    Mode = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.Mode)

class SearchSpace(luigi.WrapperTask):
    SearchSpace = luigi.Parameter()
    
    def output(self):
        return luigi.LocalTarget(self.SearchSpace)

class HitNumber(luigi.WrapperTask):
    HitNumber= luigi.IntParameter()

    def output(self):
        return luigi.LocalTarget(self.HitNumber)

class OutputPath(luigi.WrapperTask):
    OutputPath = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.OutputPath)

class CDHitCutoff(luigi.WrapperTask):
    CDHitCutoff = luigi.Parameter(default="0.9")

    def output(self):
        return luigi.LocalTarget(self.CDHitCutoff)

class Threads(luigi.WrapperTask):
    Threads = luigi.Parameter(default="1")

    def output(self):
        return luigi.LocalTarget(self.Threads)

class Aligner(luigi.WrapperTask):
    Aligner = luigi.Parameter(default="muscle")

    def output(self):
        return luigi.LocalTarget(self.Aligner)

class Tree(luigi.WrapperTask):
    Tree = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.Tree)

class DataType(luigi.WrapperTask):
    DataType = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.DataType)

class Bootstrap(luigi.WrapperTask):
    Bootstrap = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.Bootstrap)

class SubModel(luigi.WrapperTask):
    SubModel = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.SubModel)

#class GetParams(luigi.WrapperTask):
    #Params = luigi.DictParameter(default={})
    #def output(self):
        #at_params = = dict(self.to_str_params())
        #at_params['Params']=self.Params
        #return luigi.LocalTarget(self.Params)