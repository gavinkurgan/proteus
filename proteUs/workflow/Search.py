#!/usr/bin/env python3
from .DataModel import *
from ..search import NCBI_WebBlaster, XML_Parser, CDHit

@requires(TargetPath)
@requires(Mode)
@requires(HitNumber)
@requires(SearchSpace)
@requires(OutputPath)
@requires(CDHitCutoff)
@requires(Aligner)
@requires(Tree)
@requires(DataType)
@requires(Bootstrap)
@requires(SubModel)
class WebBlast(luigi.Task, NCBI_WebBlaster):
    # Requires a protein input currently
    def run(self):
        os.makedirs(self.OutputPath, exist_ok=True)
        self.compose_search_command(self.TargetPath, self.SearchSpace, self.HitNumber, self.OutputPath)
    def output(self):
        return luigi.LocalTarget(self.xml_filename(self.OutputPath, self.TargetPath, self.HitNumber, self.SearchSpace))

@requires(WebBlast)
class WebParse(luigi.Task, XML_Parser):

    def run(self):
        self.run_xml_parse(self.OutputPath, self.TargetPath, self.HitNumber, self.SearchSpace)
    def output(self):
        return {"parsed":luigi.LocalTarget(self.input().path + ".parsed.fasta")}


@requires(WebParse)
class CDHitCleanup(luigi.Task, CDHit):

    def run(self):
        print(self.input()["parsed"].path + "*"*500)
        self.run_cdhit(self.input()["parsed"].path, self.CDHitCutoff)
    def output(self):
        return {"clustered":luigi.LocalTarget((self.cd_filename(self.input()["parsed"].path, self.CDHitCutoff)))}






