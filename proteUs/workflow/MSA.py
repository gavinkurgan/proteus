#!/usr/bin/env python3
from ._base import *
from .DataModel import *
from .Search import *
from ..align import Muscle
from ..format_tools import FastaToPhylip

@requires(CDHitCleanup)
class MuscleAlign(luigi.Task, Muscle):
    def run(self):
        self.run_muscle(self.input()["clustered"].path)
    def output(self):
        return {"muscle" : luigi.LocalTarget(os.path.join(self.OutputPath, 
                                                          self.muscle_filename(self.input()["clustered"].path)
                                                          )
                                                        )}
#@inherits(CDHitCleanup)   
@requires(MuscleAlign)
class MakeMSA(luigi.Task, FastaToPhylip):
    aligners = {"muscle" :MuscleAlign}
    def requires(self):
        self.clone(self.aligners[self.Aligner])
    def run(self):
        self.fasta_to_phylip(self.input()[self.Aligner].path, self.output()["phy"].path)
    def output(self):
        return {"phy" : luigi.LocalTarget(self.phylip_name(self.input()[self.Aligner].path))}
                
