import argparse
import yaml
import json
import os
import subprocess
import proteUs
import luigi


def compose_luigi_cmd(command, local, workers):
    cmds = []
    cmd = ['luigi', '--module', 'proteUs']
    if local:
        cmd += ['--local-scheduler']
    if workers:
        cmd += ["--workers"]
        cmd += [workers]
    HOME = os.environ['HOME']
    for endpoint, flags in command.items():
        p = cmd + [endpoint]
        for param, v in flags.items():
            print(param, v)
            p.append("--{}".format(param))
            val = str(v)

            if val.lower() not in ['true', 'y']:
                if isinstance(v, list) or isinstance(v, dict):
                    list_to_str = json.dumps(v)
                    list_to_str.replace("'", '"')
                    list_to_str = list_to_str.replace('$HOME', HOME)
                    p.append(list_to_str)
                else:
                    val = val.replace('$HOME', HOME)
                    p.append(str(val))
        cmds.append(p)
    return cmds

def print_luigi_cmds(command,local, workers):
    cmds = compose_luigi_cmd(command, local,workers)
    cmd_strs = []
    for p in cmds:
        p_str = " ".join(p)
        cmd_strs.append(p_str)
    return cmd_strs

def run_luigi_dict(command, local, workers):
    cmds = compose_luigi_cmd(command,local,workers)
    for p in cmds:

        print(p)

        process = subprocess.Popen(p, shell=False, universal_newlines=True)
        process.wait()

# This allows processing multiple commands from a single yaml file
def run_luigi(yaml_file, local, workers):
    f = open(yaml_file)
    commands = yaml.safe_load_all(f)

    if commands:
        for x in commands:
            run_luigi_dict(x, local, workers)
        f.close()

    else:
        print("Empty yaml file")
        #TODO: other checks for yaml correctness

if __name__ =="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--yaml_file", help="yaml luigi run file")
    parser.add_argument("-l", "--local", action='store_true')
    parser.add_argument("-p", "--workers", help="luigi workers assigned (integer)")
    args = parser.parse_args()
    #print(args.yaml_file)
    if args.yaml_file:
        run_luigi(args.yaml_file, args.local, args.workers)
    else:
        raise Exception("at least one argument is required")

