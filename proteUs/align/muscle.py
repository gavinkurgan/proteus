from ._base import _Aligner
import subprocess

class Muscle(_Aligner):
    COMMAND = "muscle"
    

    def run_muscle(self, fasta):
        """
        Takes a fasta input of DNA or Protein sequences and 
        aligns them using muscle algorithm
        """

        ###Parameters
        #
        # -in = input fasta
        # -out = output name (fasta)
        # -c = sequence identity threshold (default = 0.9) aka "cutoff"
        # -T - # of threads
        # -n = word length (default = 5) -- see users guide to change this

        input_flag = "-in"
        output_flag = "-out"
        output_file = fasta.rstrip(".fasta") + "." + self.COMMAND + ".fasta"
        

        muscle_args = [self.COMMAND]
        muscle_args = muscle_args + [input_flag, 
                                        fasta,
                                        output_flag,
                                        output_file]

#        if params:
#            for k,v in params.items():
#                if v == None:
#                    muscle_args += [k]
#                else:
#                    muscle_args += [k, v]

        subprocess.call(muscle_args)

    def muscle_filename(self, fasta):
        return fasta.rstrip(".fasta") + "." + self.COMMAND + ".fasta"