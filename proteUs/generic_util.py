from functools import wraps
import errno
import os
import signal
import hashlib
import subprocess
from subprocess import PIPE
from .proteUs_logging import logger


class TimeoutError(Exception):
    pass


def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator


def md5_for_file(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def update_md5(md1, other):
    x = hashlib.md5()
    x.update(md1.encode('utf-8'))
    x.update(other.encode('utf-8'))
    return x.hexdigest()


def _timeit(f):
    import time
    import datetime

    @wraps(f)
    def _decorator(*args, **kw):
        logger.debug("Started {} at {}".format(f.__name__,
                                        datetime.datetime.now()))

        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        logger.debug("Finished {} at {}".format(f.__name__,
                                         datetime.datetime.now()))

        logger.debug('{} total time: {}'.format(f.__name__, te - ts))
        return result

    return _decorator

def map_list_to_str(f):
    @wraps(f)
    def _decorator(*args, **kw):
        result = f(*args, **kw)
        return list(map(str, result))
    return _decorator


def touch(fname, times=None):
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, times)
    finally:
        fhandle.close()

def check_file_size_is_greater_than(path, filesize=0):

    if os.path.exists(path) and os.path.getsize(path)>filesize:
        return True
    else:
        return False

@_timeit
def run_subprocess(command_line_list,
                   output_filehandle=None,
                   stdin_filehandle=None,
                   stderr_filehandle=None,
                   wait=True,
                   docker=None):

    # TODO: Logging

    logger.info(command_line_list)

    #Global variables to be set by config file
    RUN_DOCKER = os.getenv('SEQTOOLS_DOCKER', False) == 'TRUE'
    SEQTOOLS_DATA = os.getenv("SEQTOOLS_DIR", "")

    #eg
    #docker run -v ~/data:/data salmon_alpine:0.8.1 salmon index --transcripts /data/Mcap_rnaseq/references/mcap_NC_002977_genes.fasta --index /data/Mcap_rnaseq/references/delme

    if RUN_DOCKER:
        command_line_list = [i.replace(SEQTOOLS_DATA, "/data") for i in command_line_list if isinstance(i, str)]
        command_string = " ".join(command_line_list)
        command_line_list = ["docker", "run", "-v", "{}:/data".format(SEQTOOLS_DATA) , docker,  "/bin/sh -c "]+['"'+command_string+'"']
        command = " ".join([ i for i in command_line_list if i is not None])
    else:
        command = " ".join([ i for i in command_line_list if i is not None])
    logger.info("Running: {}".format(" ".join(command_line_list)))
    logger.info("Running: {}".format(command))


    #unfortunately due to the way we construct the commands for docker, we must use shell=True here
    p = subprocess.Popen(command, shell=True, universal_newlines=True,
                         stdout=output_filehandle, stdin=stdin_filehandle,
                         stderr=stderr_filehandle)
    if wait:
        p.communicate()
        if not p.poll() == 0:
            # TODO: Catch reason for failure
            raise ChildProcessError('Failed process: "{}"'.format(command_line_list))

    return p

def add_default_param(command_line_list,
                     param_dict,
                     flag,
                     value):
        '''todo
        Need to support aliases
        eg in freebayes,
        -4 is the same as --use-duplicate-reads'''
        if flag not in param_dict:
            if value:
                command_line_list += [flag, value]
            else:
                command_line_list += [flag]
        return command_line_list
