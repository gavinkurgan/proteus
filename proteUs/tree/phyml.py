from ._base import _Tree
import subprocess

class PhyML(_Tree):
    COMMAND = "phyml"
    

    def run_phyml(self, phylip, data_type, bootstraps, model_used):
        """
        Takes a .phy input of DNA or Protein sequences and 
        makes a phylogenetic tree using phyml construction algorithm.
        """

        ###Parameters
        #
        # -i = input phylip
        # -d = data type (aa or nt)
        # -b = bootstrap value
        # -m = Substitution model name

        input_flag = "-i"
        data_flag = "-d"
        bootstrap_flag = "-b"
        model_flag = "-m"
        

        phyml_args = [self.COMMAND]
        phyml_args = phyml_args + [input_flag, 
                                        phylip,
                                        data_flag,
                                        data_type,
                                        bootstrap_flag,
                                        bootstraps,
                                        model_flag,
                                        model_used]

#        if params:
#            for k,v in params.items():
#                if v == None:
#                    phyml_args += [k]
#                else:
#                    phyml_args += [k, v]

        subprocess.call(phyml_args)

    def phyml_filename(self, phylip):
        return phylip +"_phyml_tree.txt"