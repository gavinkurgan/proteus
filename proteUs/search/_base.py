import os
import subprocess
from proteUs.generic_util import run_subprocess, map_list_to_str
from ..proteUs_logging import logger

class _Search(object):
    """
    Base class for searching/parsing functions
    """
    def sample_id(self, TargetPath):

        return os.path.splitext(os.path.basename(TargetPath).rstrip(".fasta"))[0]

    def xml_filename(self, OutputPath, TargetPath, HitNumber, SearchSpace):

    	return os.path.join(OutputPath, self.sample_id(TargetPath) + "." + str(HitNumber) +"." + SearchSpace + ".xml")
    