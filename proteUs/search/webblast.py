from ._base import _Search
import os
from Bio.Blast import NCBIWWW
from Bio import SeqIO

class NCBI_WebBlaster(_Search):

    def compose_search_command(self,
                                TargetPath,
                                SearchSpace,
                                HitNumber,
                                OutputPath,
                                threads=1,
                                params={}
                                ):

        record = SeqIO.read(TargetPath, format="fasta")
        #Reads single record as fasta format **Change this if doing diff alignment
        result_handle = NCBIWWW.qblast("blastp", "nr", record.format("fasta"),entrez_query= SearchSpace, expect=0.001, hitlist_size= int(HitNumber))
        #Blasts this sequence(blastx) against nr protein database
        save_file = open(self.xml_filename(OutputPath, TargetPath, HitNumber, SearchSpace), "w")
        save_file.write(result_handle.read())
        #Saves file full of xml blast results. Ready for ncbi_webparser.py
        save_file.close()
        result_handle.close()