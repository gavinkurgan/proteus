from ._base import _Search
from Bio.Blast import NCBIXML
from Bio import SeqIO
import re
import sys

class XML_Parser(_Search):

    def run_xml_parse(self, OutputPath, TargetPath, HitNumber, SearchSpace):
        E_VALUE_THRESH= 0.001


        xml_file = self.xml_filename(OutputPath, TargetPath, HitNumber, SearchSpace)
        #xml_file = str(os.path.join(OutputPath, str(self.sample_id(TargetPath)) +".xml"))
        with open(xml_file) as infile:
            output_file = xml_file + ".parsed.tab"
            with open(output_file, "w") as tab_output:

                min_seq_len = self.get_template_length(TargetPath)
                blast_record = NCBIXML.read(infile)
                for alignment in blast_record.alignments:
                    for hsp in alignment.hsps:
                        if hsp.expect < E_VALUE_THRESH:
                            name = alignment.title.replace("gi|", "")
                            seq = hsp.sbjct.replace("-","")
                            if not re.search("MULTISPECIES", name):
                                if not re.search("partial", name):
                                    if (len(seq) > min_seq_len):
                                        tab_output.write(name + "\t" + seq + "\n")
            self.convert_tab_to_fasta(output_file, xml_file)

                                    #This part forms a for loop that grabs each blast alignment, takes its title
                                    #(a) and eliminate gi| (to allow unique conversion for phylip format later
                                    #it also grabs the querys sequence (c).
                                    #This removes all "multispecies" sequences before outputting the results
                                    #In a tab format that is easily converted to fasta

    def get_template_length(self, TargetPath):
        sequence_length_cutoff = int(0.85)
        for record in SeqIO.parse(TargetPath, "fasta"):
            min_seq_len = (sequence_length_cutoff * len(record.seq))
            return min_seq_len

    def convert_tab_to_fasta(self, Tab_Sequences, XML_file_name):
        SeqIO.convert(Tab_Sequences, "tab", XML_file_name + ".parsed.fasta", "fasta")

