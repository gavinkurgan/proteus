from ._base import _Search, run_subprocess, map_list_to_str
import subprocess

class CDHit(_Search):
    COMMAND = "cd-hit"
    

    def run_cdhit(self, fasta_hits, cutoff):
        """
        Takes a fasta input of DNA or Protein sequences and 
        compares/clusters sequences based on similarity
        """

        ###Parameters
        #
        # -i = input fasta
        # -o = output name
        # -c = sequence identity threshold (default = 0.9) aka "cutoff"
        # -T - # of threads
        # -n = word length (default = 5) -- see users guide to change this

        input_flag = "-i"
        cutoff_flag = "-c"
        output_flag = "-o"
        output_file = fasta_hits.rstrip(".fasta") + "." + str(int(float(cutoff)*100)) + ".clustered.fasta" 

        cdhit_args = [self.COMMAND]
        cdhit_args = cdhit_args + [input_flag, 
                                        fasta_hits,
                                        cutoff_flag,
                                        cutoff,
                                        output_flag,
                                        output_file]

#        if params:
#            for k,v in params.items():
#                if v == None:
#                    cdhit_args += [k]
#                else:
#                    cdhit_args += [k, v]




        subprocess.call(cdhit_args)

    def cd_filename(self, fasta_hits, cutoff):
        return fasta_hits.rstrip(".fasta") + "." + str(int(float(cutoff)*100)) + ".clustered.fasta"

