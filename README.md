####Currently under development
ProteUs is currently under development and is not functionally ready to perform many of the goals of the project. Thanks for your patience with any bugs that may be within.

####Goal
ProteUs is designed to be a set of computational sequence-based protein engineering tools to allow users to quickly identify potential sites for targeted mutagenesis or screening of active variants for a function of interest. It is set up in a way to try and allow easy cross-talk between tools and minimize repeat of time-consuming sequence gathering steps.

####Installation: v1 


Create a new environment using conda, then install the proteus module
```
conda create -n proteUs python=3.6 anaconda pip
```
You will then need to clone the repo using git clone and activate your environment
```
source activate proteUs
#activate the proteus conda environment
```
You will then need to create the proteUs module within this environment:

```
pip install . #inside the proteus code dir
#this will use static snapshot of code (needs to be updated to sync up with module)
```

if you want a development install, use pip install -e .


***NOTE the development install doesn't currently work, this seems to be a known issue pip recognizing anaconda envs. Instead, to update code during development just run:

```
pip install -I --no-deps /proteus
```

###Install for Local execution
This requires that all tools be available on the $PATH.

```
conda install --quiet --yes --channel conda-forge \
argparse=1.4.0 \
cd-hit=4.6.8 \
luigi=2.6.2 \
muscle=3.8.1551 \
phyml=3.3.20170530 \
pyyaml=3.12 \
yaml=0.1.6-0 \

```

####How to use
Proteus is designed to allow flexible manipulation of large sequence comparisons by a myriad of tools for protein engineering. Currently, it is only capable of searching, clustering, aligning, and building a phylogenetic tree for sequence comparison. This is useful as these kinds of files can easily be submitted to online webapps like CAPS to attempt to predict amino acids participating in co-evolution. At the moment it is up to the end-user to explore how to use these for protein engineering. More to come. 

However, to run these analyses, a luigi helper script (mario.py) must be called together with a config yaml file.
```
python mario.py -l -i yaml_configs/CAPS.yaml
```
-i designates the input config .yaml

-l signals the script to run luigi locally. To allow luigi to run with luigi task visualizer output remove this flag and make sure luigid is running.

-p number of luigi workers assigned (not much use if not in a batch mode)

To do this run in a separate terminal
```
luigid
```
You can access the output typically at port 8082. In a browser type:
```
HOST:8082
```

### The config file
The config file still currently is under development. Non-essential parameter passing will eventually be permitted using the parameters section at the botttom of the config. The CAPS.yaml is structured as such:
```
MakeTree:  																#final program luigi looks to be able to run.
#Global Params
    TargetPath: /home/gkurgan/proteus_tests/test/acrB.fasta 			#user input .fasta file
    Mode: CAPS
    SearchSpace: Bacteria 												#organismal constrains for searching sequence homologs
    HitNumber: 200 # number of sequences to return
    OutputPath: /home/gkurgan/proteus_tests/test/acrB 				# output directory
    Aligner: muscle 												#MSA aligner
#Clustering Params
    CDHitCutoff: 0.90 												#similarity needed to be clustered (0.90 = 90%)
#TreeBuilding Params
    DataType: aa 													# sequence type (aa or nt)
    Bootstrap: 10 													# bootstrap number for treeforming
    SubModel: LG 													# substitution model
    Tree: phyml 													# tree program
#    Params: 														#currently in development
#        - cdhit:
#            - c:0.0
#        - webblast:
#            - b:5
```


####Features
Luigi will keep track of dependencies and check if output files exist.  Custom functions to check output file formats, etc can also be written.

The modular nature of how proteus is setup should allow easy interchangeable parts in pipelines.  

####Wishlist
```
*Coevolution predictions using multiple programs and merging of mutual clusters
*Ancestral reconstruction or REAP for protein engineering
*Point mutation abundance analysis for finding other naturally functional sequences of interest
*More aligners
*More tree building algorithms
```

####Luigi notes
Make sure you understand the requires and inherits decorators, they are useful in passing parameters around.

http://luigi.readthedocs.io/en/stable/api/luigi.util.html

